#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL/SDL.h>
#include "functions.h"

int main()
{
    SDL_Surface *ecran = NULL;
    SDL_Init(SDL_INIT_VIDEO);

    ecran = SDL_SetVideoMode(600, 600, 32, SDL_HWSURFACE);

    if (ecran == NULL)
    {
        fprintf(stderr, "Error Init SDL : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    printf("Success : Init SDL\n");
    /* Init Fenetre */
    SDL_SetVideoMode(600, 600, 32, SDL_HWSURFACE | SDL_RESIZABLE | SDL_DOUBLEBUF);
    SDL_WM_SetCaption("Math", NULL);


    /* Affichage Fenetre Blanche */
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
    
    // Création d'un point
    creation_point(300, 300, ecran);
    creation_trait(200, 400, 375, 425, ecran, 300, 400);
    creation_cercle(300, 300, ecran);
    
    pause();


    SDL_Quit();
    return EXIT_SUCCESS;
}


