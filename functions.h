#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED


void pause();
void creation_point(int x, int y, SDL_Surface *ecran);
void creation_point_bleu(int x, int y, SDL_Surface *ecran);
void creation_trait(int x1, int x2, int y1, int y2, SDL_Surface *ecran, int origineX, int origineY);
void creation_cercle(int centreX, int centreY, SDL_Surface *ecran);


#endif