#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <math.h>
#define taille 20
#define nbre 4

void creation_cercle(int coo[],SDL_Surface *ecran);
int main(int argc, char *argv[])

{
    int j,coo[2]= {200,0}; //matrice des coordonn�es;
    SDL_Surface *ecran = NULL, *line;
    SDL_Rect position;
    SDL_Init(SDL_INIT_VIDEO);
    ecran = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE);
    SDL_WM_SetCaption("les petits cercles !", NULL);
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 17, 206, 112));
    SDL_Flip(ecran);
    int done = 0;
    while (!done) //boucle de fermeture
    {
        for (j=0;j<=100; j++)
        {
            coo[1]=j;
          SDL_Delay(10);
            SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
            creation_cercle(coo,ecran);
        }
   line=SDL_CreateRGBSurface(SDL_HWSURFACE,1,100,32,0,0,0,0);
   SDL_FillRect(line, NULL, SDL_MapRGB(ecran->format,255 ,0,0));
                position.x=100;
                position.y=10;
                SDL_BlitSurface(line, NULL, ecran, &position); // Collage de la surface sur l'�cran
                   SDL_Flip(ecran);
                   SDL_Delay(1000);
        SDL_Event event;
        while (SDL_PollEvent(&event))//rappel de cette fonction tant q'un �v��nement ne s'est pas produit SDL_PollEvent
            // utiliser cela quand il y a du mouvement sinon privil�gier : SDL_WaitEvent qui attend l'�v�nement pour agir.
        {
            // check for messages
            switch (event.type)
            {
            // exit if the window is closed
            case SDL_QUIT:
                done = 1;
                break;

            // check for keypresses
            case SDL_KEYDOWN:
            {
                // exit if ESCAPE is pressed
                if (event.key.keysym.sym == SDLK_ESCAPE)//on peut noter des lettres ou des touches
                    done = 1;
                break;
            }
                /* case SDL_MOUSEMOTION:
                 {//la souris bouge
                 coo[0]=event.motion.x; //coordonn�es x  de la souris
                 coo[1]=event.motion.y;//coordon�es y de la souris
                 creation_cercle(coo,ecran);
                 }*/
                /*         case SDL_MOUSEBUTTONDOWN:
                         {//il y a un clic
                             if( event.button.button == SDL_BUTTON_LEFT )// c'est le bouton gauche
                        {coo[0]=event.button.x; //coordonn�es x  de la souris
                         coo[1]=event.button.y;//coordon�es y de la souris

                     SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
                         SDL_Flip(ecran);
                         creation_cercle(coo,ecran);
                         }
                         }
                         */

            } // end switch
        } // end of message processing
    }



//   SDL_Delay(100000);// cr�e une tempo


//   SDL_FreeSurface(rectangle); // Lib�ration de la surface

    SDL_Quit();


    return EXIT_SUCCESS;

}
void creation_cercle(int coo[],SDL_Surface *ecran)
{
    SDL_Surface *cercle[nbre][taille][taille];
    SDL_Rect position;
    int i, j, k;
    for (i=0; i<taille; i++)
    {
        for (j=0; j<taille; j++)
        {
            if (sqrt(pow(i,2)+pow(j,2))<=taille)
            {
                for (k=0; k<nbre; k++)
                {
                    cercle[k][i][j] = SDL_CreateRGBSurface(SDL_HWSURFACE, 1, 1, 32, 0, 0, 0, 0);
                }
                SDL_FillRect(cercle[0][i][j], NULL, SDL_MapRGB(ecran->format, 255,0,0));//1er quart de cercle
                position.x=i+coo[0];
                position.y=j+coo[1];
                SDL_BlitSurface(cercle[0][i][j], NULL, ecran, &position); // Collage de la surface sur l'�cran

                SDL_FillRect(cercle[1][i][j], NULL, SDL_MapRGB(ecran->format, 255,0,0));//2�me quart de cercle
                position.x=-j+coo[0];
                position.y=i+coo[1];
                SDL_BlitSurface(cercle[1][i][j], NULL, ecran, &position); // Collage de la surface sur l'�cran

                SDL_FillRect(cercle[2][i][j], NULL, SDL_MapRGB(ecran->format, 255,0,0));//3eme quart de cercle
                position.x=-i+coo[0];
                position.y=-j+coo[1];
                SDL_BlitSurface(cercle[2][i][j], NULL, ecran, &position); // Collage de la surface sur l'�cran

                SDL_FillRect(cercle[3][i][j], NULL, SDL_MapRGB(ecran->format, 255,0,0));//4eme quart de cercle
                position.x=j+coo[0];
                position.y=-i+coo[1];
                SDL_BlitSurface(cercle[3][i][j], NULL, ecran, &position); // Collage de la surface sur l'�cran
            }
        }
    }



    //     creation_cercle(coo,ecran);
    SDL_Flip(ecran);
// Mise � jour de l'�cran
}

