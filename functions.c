// Commande pour compiler : gcc main.c functions.c -o Programme `sdl-config --cflags --libs` -lm
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL/SDL.h>
#define AGRANDISSEMENTC 1.2
#define ROTATIONB 150

#include "functions.h"

void pause()
{
    int continuer = 1;
    SDL_Event event;
    while (continuer)
    {
        SDL_WaitEvent(&event);
        switch (event.type)
        {
        case SDL_QUIT:
            continuer = 0;
        }
    }
}

void creation_point(int x, int y, SDL_Surface *ecran){
    SDL_Surface *point; 
    SDL_Rect position;

    point = SDL_CreateRGBSurface(SDL_HWSURFACE, 2, 2, 32, 0, 0, 0, 0);
    SDL_FillRect(point, NULL, SDL_MapRGB(ecran->format, 255, 0, 0));
    position.x = x;
    position.y = y;
    SDL_BlitSurface(point, NULL, ecran, &position);

    //printf("Point créé en : [%d,%d]\n", position.x, position.y);
    SDL_Flip(ecran);
}

void creation_point_bleu(int x, int y, SDL_Surface *ecran){
    SDL_Surface *point; 
    SDL_Rect position;

    point = SDL_CreateRGBSurface(SDL_HWSURFACE, 2, 2, 32, 0, 0, 0, 0);
    SDL_FillRect(point, NULL, SDL_MapRGB(ecran->format, 0, 0, 255));
    position.x = x;
    position.y = y;
    SDL_BlitSurface(point, NULL, ecran, &position);

    //printf("Point créé en : [%d,%d]\n", position.x, position.y);
    SDL_Flip(ecran);
}

void creation_trait(int x1, int x2, int y1, int y2, SDL_Surface *ecran, int origineX, int origineY){
    double x = x2 - x1;
    double y = y2 - y1;
    double longueur = sqrt(x*x + y*y);
    double rotation = ROTATIONB;
    double pi = acos(-1.0);
    double vrotate = pi / 180.0;
    double aX = x / longueur;
    double aY = y / longueur;
    x = x1;
    y = y1;

    for(int i = 0 ; i < longueur; i++){
        double posX = cos(rotation * vrotate) * (x - origineX) - sin(rotation * vrotate) * (y - origineY) + origineX; 
        double posY = sin(rotation * vrotate) * (x - origineX) + cos(rotation * vrotate) * (y - origineY) + origineY;
        
        printf("posX : %f - posY : %f\n", posX, posY);
        creation_point(posX, posY, ecran);
        creation_point_bleu(x, y, ecran);
        y += aY;
        x += aX;
    }
    printf("PI : %f\n", pi);
    printf("Longueur du Trait %f\n", longueur);
}

void creation_cercle(int origineX, int origineY, SDL_Surface *ecran) {
    double x, y, r2;
    int rayon = 180;
    r2 = rayon * rayon;
    double partieNonDessine = origineX - sqrt(3)/2 * rayon;
    for(x = rayon; x >= -rayon; x -= 0.3){
        y = (int) (sqrt(r2 - x*x) + 0.5);
        double valeurAgrandissement = AGRANDISSEMENTC;
        double alignementX = origineX * valeurAgrandissement;
        alignementX -= origineX;
        
        double alignementY = origineY * valeurAgrandissement;
        alignementY -= origineY;
        /* La var Alignement permet de faire une homotétie sans avoir le bug de l'alignement */
        double posX = valeurAgrandissement * (x + origineX) - alignementX;
        double posY = valeurAgrandissement * (y + origineY) - alignementY;

        // Sans l'homotétie
        creation_point_bleu(x + origineX , y + origineY, ecran);
        // Avec Homotétie
        creation_point((posX), (posY), ecran);
        
        posX = valeurAgrandissement * (origineX - x) - alignementX;
        posY = valeurAgrandissement * (origineY - y) - alignementY;
        if (origineY - y > partieNonDessine){
            creation_point_bleu(origineX - x , origineY - y, ecran);
            creation_point(posX, posY, ecran);
        }
    }
        printf("Valeur de la partie ND : %f\n", partieNonDessine);
}
